import string
import random

mutation_rate = 0.0001
runs = 10
population_size = 200
max_generations = 1000
alphabet = list(string.letters[0:26])

def fitness(s):
    # Calculate fitness as the number of characters that are in the right spot for the alphabet.
    return sum(ch1 == ch2 for ch1, ch2 in zip(s, alphabet)) / float(len(alphabet))

def getFitness(item):
    return item[0]

def randitem(l):
    # Choose an item probabilistically according to the scores provided in column a.
    r = random.uniform(0.0, sum([a for (a,b,c) in l]))
    s = 0
    for a,b,c in l:
        s += a
        if r <= s:
            return b
    return random.choice(l)[1]

def mate(s1, s2):
    return [mutate(s1[i] if random.uniform(0, 1.0) > 0.5 else s2[i]) for i in range(len(s1))]

def mutate(c):
    return c if random.uniform(0.0, 1.0) >= mutation_rate else random.choice(alphabet)

def evolve(ss):
    generations = 0
    while generations < max_generations:
        generations += 1
        # Get fitness
        fits = [[fitness(s), s] for s in ss]
        # Calculate relative fitness as compared to the population minimum.
        minf = min([f[0] for f in fits])
        nfits = [[(f[0] - minf), f[1], f[0]] for f in fits]
        if alphabet in ss:
            return generations, fits[ss.index(alphabet)]
        # Mate probabilistically according to fitness.
        ss = [mate(randitem(nfits), randitem(nfits)) for _ in ss]
    return generations, sorted(fits, key=getFitness)[-1]


wins = 0
tot = 0
time = 0
score = 0
for r in range(runs):
    strings = [[random.choice(alphabet) for letter in range(len(alphabet))] for i in range(population_size)]
    generations, best = evolve(strings)
    print 'Run', r, 'Time (generations):', generations, 'Best score:', best[0], 'Best sequence:', best[1]
    score += best[0]
    if best[1] == list(alphabet):
        time += generations
        wins += 1.0
print 'WIN %:', wins / runs
print 'AVG TIME FOR WINS:', -1 if time == 0 else time / wins
print 'AVG SCORE OVERALL:', score / runs
